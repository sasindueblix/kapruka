<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('promotion');
            $table->string('title');
            $table->string('family_name');
            $table->string('name');
            $table->string('citizen');
            $table->date('birthday');
            $table->string('birth_place');
            $table->string('occupation');
            $table->string('email', 190)->unique();
            $table->string('password');
            $table->string('telephone');
            $table->string('mobile');
            $table->string('street_num');
            $table->string('street_name');
            $table->string('street_type');
            $table->string('post_code');
            $table->string('suburb');
            $table->string('state');
            $table->string('id_type');
            $table->string('id_country');
            $table->string('id_num');
            $table->string('id_exp');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
