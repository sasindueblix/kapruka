@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="">
            <div class="panel panel-default">
                <div style="color: #333; text-align: center; font-family: 'Yanone Kaffeesatz', sans-serif; width: 100%;font-size: 20px; border-bottom: 1px solid #333;">Bank Account Details</div><br>

                <div class="panel-body">

                    <div class="container">

                        <div class="row bank_details_row">
                            <div class="col-md-3">
                                National Australia Bank
                            </div>
                            <div class="col-md-3">
                                Flexewallet Kapruka
                            </div>
                            <div class="col-md-3">
                                083 004
                            </div>
                            <div class="col-md-3">
                                83717 5590
                            </div>
                        </div>

                        <p>There are, however, few conditions attached to this new facility. These are:</p>

                        <ul>
                            <li><p>No cash deposits.</p></li>
                            <li><p>All internet transfers should have your customer ID as reference. This is mandatory. </p></li>
                            <li><p>As strict compliance guide lines are to be followed, remittances will be carried out on the following day. If needed our compliance officer Chamaka Manjula may call you for further documentary evidence like proof of income, purpose of transfer, beneficial ownership and your relationship to the receiver. </p>
                                <p>We have to obtain your new photo ID in digital form to be input to our new IT platform. We already have many IDs recorded. Please call us to get this reconfirmed. The best photo ID is an Australian driving license. In case you do not have an Australian driving license, a copy of your passport and a copy of a utility bill will be required as proof of address.</p>
                            </li>
                            <li><p>Your Sri Lankan accounts will be credited the following day by 9 am, however only in case of emergencies we can consider crediting the accounts on the same day – based on the merit of the case. We may require documents.</p></li>
                            <li><p>From 1st July 2017,  please do not use any other account that we have provided you in the past.</p></li>
                        </ul>
            
                        <p>We thank you for your support and custom given to us during the past 18 years. In particular, I am grateful to you for sticking with us during the last 2 years when Australian banks started closing  accounts of all remittance companies.  We are moving your data to a new computer platform to meet with the compliance requirements of NAB, Flexewallet and AUSTRAC. We shall absorb Flexewallet fees and the fee structure which we have maintained for the past 18 years will remain the same.</p>
                        <p>May I  request your continued support and co-operation to satisfy enhanced due diligence of customers and to comply with the increased demands placed on us by NAB, Flexewallet and the Australian Government due to compliance requirements.</p>
                        <p>Please ensure that you put your customer ID in the reference field when you do Internet Transfers. Without this, we may not be able to trace your payment and thus delay your transfer.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
