@extends('layouts.admin')

@section('content')
<div class="page-wrapper">
    <div class="row">
        <div class="col-md-2">
            <div class="panel panel-default">
                <div class="panel-heading">DASHBOARD</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif                    
                    <a href="{{ route('users') }}">Users</a>
                </div>
            </div>
        </div>
        <div class="col-md-8 col-md-offset-1">

            <div class="panel panel-default">
                <div style="color: #5599dd; text-align: center; font-family: 'Yanone Kaffeesatz', sans-serif; width: 100%;font-size: 20px; background-color:rgb(247, 247, 255);">Users</div><br>
                    <table>
                        <tr>
                            <th class="col-md-2"><strong>No</strong></th>
                            <th class="col-md-2"><strong>Name</strong></th>
                            <th class="col-md-2"><strong>Email</strong></th>
                            <th class="col-md-2"><strong>Contact</strong></th>
                        </tr>
                        @foreach($users as $user)
                            <tr style="font-size: 13px; border-bottom: 1px solid rgb(202, 228, 226);">
                                <td class="col-md-2">{{ $user->id  }}</td>
                                <td class="col-md-2">{{ $user->name  }}</td>
                                <td class="col-md-2">{{ $user->email }}</td>
                                <td class="col-md-2">{{  $user->mobile }}</td>
                                <td class="col-md-2">
                                    <a href="" class=" btn-primary btn-xs">&#10064;</a> <a href="" class=" btn-danger btn-xs delete_area" data-toggle="modal">&#9997;</a> <a href="" class=" btn-danger btn-xs delete_area" data-toggle="modal">&#10007;</a>
                                </td>
                            </tr>
                        @endforeach
                    </table><br>
            </div>

        </div>
    </div>
</div>
@endsection
