@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div style="color: #333; text-align: center; font-family: 'Yanone Kaffeesatz', sans-serif; width: 100%;font-size: 20px; border-bottom: 1px solid #333;">Profile</div><br>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <table>
                        <tr>
                            <td class="col-md-4"><strong>Name:</strong></td>
                            <td class="col-md-6">{{ Auth::user()->title }}. {{ Auth::user()->name }}</td>
                        </tr>
                        @if (\Auth::user()->role === 'user')
                        <tr>
                            <td class="col-md-4"><strong>Family Name:</strong></td>
                            <td class="col-md-6">{{ Auth::user()->family_name }}</td>
                        </tr>
                        <tr>
                            <td class="col-md-4"><strong>Citizenship: </strong></td>
                            <td class="col-md-6">{{ Auth::user()->citizen }}</td>
                        </tr>
                        <tr>
                            <td class="col-md-4"><strong>Date of Birth: </strong></td>
                            <td class="col-md-6">{{ Auth::user()->birthday }}</td>
                        </tr>
                        <tr>
                            <td class="col-md-4"><strong>Place of Birth: </strong></td>
                            <td class="col-md-6">{{ Auth::user()->birth_place }}</td>
                        </tr>
                        <tr>
                            <td class="col-md-4"><strong>Occupation: </strong></td>
                            <td class="col-md-6">{{ Auth::user()->occupation }}</td>
                        </tr>
                        @endif
                        <tr>
                            <td class="col-md-4"><strong>Email Address: </strong></td>
                            <td class="col-md-6">{{ Auth::user()->email }}</td>
                        </tr>
                        @if (\Auth::user()->role === 'admin')
                        <tr>
                            <td class="col-md-4"><strong>Password: </strong></td>
                            <td class="col-md-6" type="password"></td>
                        </tr>
                        @endif
                        @if (\Auth::user()->role === 'user')
                        <tr>
                            <td class="col-md-4"><strong>Mobile: </strong></td>
                            <td class="col-md-6">{{ Auth::user()->mobile }}</td>
                        </tr>
                        <tr>
                            <td class="col-md-4"><strong>Telephone: </strong></td>
                            <td class="col-md-6">{{ Auth::user()->telephone }}</td>
                        </tr>
                        <tr>
                            <td class="col-md-4"><strong>Street Number: </strong></td>
                            <td class="col-md-6">{{ Auth::user()->street_num }}</td>
                        </tr>
                        <tr>
                            <td class="col-md-4"><strong>Street Name: </strong></td>
                            <td class="col-md-6">{{ Auth::user()->street_name }}</td>
                        </tr>
                        <tr>
                            <td class="col-md-4"><strong>Street Type: </strong></td>
                            <td class="col-md-6">{{ Auth::user()->street_type }}</td>
                        </tr>
                        <tr>
                            <td class="col-md-4"><strong>Post Code: </strong></td>
                            <td class="col-md-6">{{ Auth::user()->post_code }}</td>
                        </tr>
                        <tr>
                            <td class="col-md-4"><strong>Suburb: </strong></td>
                            <td class="col-md-6">{{ Auth::user()->suburb }}</td>
                        </tr>
                        <tr>
                            <td class="col-md-4"><strong>State: </strong></td>
                            <td class="col-md-6">{{ Auth::user()->state }}</td>
                        </tr>
                        <tr>
                            <td class="col-md-4"><strong>ID Type: </strong></td>
                            <td class="col-md-6">{{ Auth::user()->id_type }}</td>
                        </tr>
                        <tr>
                            <td class="col-md-4"><strong>ID Number: </strong></td>
                            <td class="col-md-6">{{ Auth::user()->id_num }}</td>
                        </tr>
                        <tr>
                            <td class="col-md-4"><strong>Expiry Date: </strong></td>
                            <td class="col-md-6">{{ Auth::user()->id_exp }}</td>
                        </tr>
                        <tr>
                            <td class="col-md-4"><strong>Issued Country: </strong></td>
                            <td class="col-md-6">{{ Auth::user()->id_country }}</td>
                        </tr>                              
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
