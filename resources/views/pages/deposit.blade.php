@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('deposit') }}">
                        {{ csrf_field() }}
                        <div style="color: #333; text-align: center; font-family: 'Yanone Kaffeesatz', sans-serif; width: 100%;font-size: 20px; border-bottom: 1px solid #333;">Deposit Notice</div><br>
                        <div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
                            <label for="amount" class="col-md-4 control-label">Amount Deposited</label>

                            <div class="col-md-6">
                                <input id="amount" type="text" class="form-control" name="amount" autofocus>

                                @if ($errors->has('amount'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('amount') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('source') ? ' has-error' : '' }}">
                            <label for="source" class="col-md-4 control-label">Source of Funds</label>

                            <div class="col-md-6">
                                <select class="form-control" id="source" name="source" required="">
                                    <option disabled="" selected="">Please Select</option>
                                    <option>Personal Savings</option>
                                    <option>Business Activities</option>
                                    <option>Loan</option>
                                    <option>Family Money</option>
                                </select>

                                @if ($errors->has('source'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('source') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('purpose') ? ' has-error' : '' }}">
                            <label for="purpose" class="col-md-4 control-label">Purpose</label>

                            <div class="col-md-6">
                                <select class="form-control" id="purpose" name="purpose" required="">
                                    <option disabled="">Please Select</option>
                                    <option>Family Support</option>
                                    <option>Medical Expenses</option>
                                    <option>Personal Travels and Tours</option>
                                    <option>Repayments of Loans</option>
                                    <option>Gift</option>
                                    <option>Special Occasion</option>
                                    <option>Investment</option>
                                    <option>Goods Purchased</option>
                                    <option>Other</option>
                                </select>

                                @if ($errors->has('purpose'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('purpose') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('beneficiary') ? ' has-error' : '' }}">
                            <label for="beneficiary" class="col-md-4 control-label">Beneficiary</label>

                            <div class="col-md-6">
                                <select class="form-control" id="beneficiary" name="beneficiary" required="">
                                    <option disabled="" selected="">Please Select</option>
                                    <option>Parent</option>
                                    <option>Spouse</option>
                                    <option>Friend</option>
                                    <option>Child</option>
                                    <option>Brother</option>
                                    <option>Sister</option>
                                    <option>Grand Parent</option>
                                    <option>Uncle</option>
                                    <option>Aunt</option>
                                    <option>Cousin</option>
                                    <option>Nephew</option>
                                    <option>Niece</option>
                                    <option>In-law</option>
                                    <option>Own</option>
                                </select>

                                @if ($errors->has('beneficiary'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('beneficiary') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('satate') ? ' has-error' : '' }}">
                            <label for="satate" class="col-md-4 control-label">Payment Method</label>

                            <div class="col-md-6">
                                <select class="form-control" id="state" name="state" required="">
                                    <option disabled="" selected="">Please Select</option>
                                    <option>Account Deposit</option>
                                    <option>Over The Counter</option>
                                </select>

                                @if ($errors->has('satate'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('satate') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
