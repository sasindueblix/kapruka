@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('beneficiaries') }}">
                        {{ csrf_field() }}
                        <div style="color: #333; text-align: center; font-family: 'Yanone Kaffeesatz', sans-serif; width: 100%;font-size: 20px; border-bottom: 1px solid #333;">BENEFICIARY DETAILS</div><br>

                        <div class="form-group{{ $errors->has('ben_name') ? ' has-error' : '' }}">
                            <label for="ben_name" class="col-md-4 control-label">Name without Initials</label>

                            <div class="col-md-6">
                                <input id="ben_name" type="text" class="form-control" name="ben_name" value="{{ old('ben_name') }}" required>

                                @if ($errors->has('ben_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('ben_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('sl_address') ? ' has-error' : '' }}">
                            <label for="sl_address" class="col-md-4 control-label">Sri Lankan Address</label>

                            <div class="col-md-6">
                                <input id="sl_address" type="text" class="form-control" name="sl_address" value="{{ old('sl_address') }}" required>

                                @if ($errors->has('sl_address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('sl_address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('contact') ? ' has-error' : '' }}">
                            <label for="contact" class="col-md-4 control-label">Contact Number</label>

                            <div class="col-md-6">
                                <input id="contact" type="text" class="form-control" name="contact" value="{{ old('contact') }}" required>

                                @if ($errors->has('contact'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('contact') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('acc_num') ? ' has-error' : '' }}">
                            <label for="acc_num" class="col-md-4 control-label">Account Number</label>

                            <div class="col-md-6">
                                <input id="acc_num" type="text" class="form-control" name="acc_num" value="{{ old('acc_num') }}" required>

                                @if ($errors->has('acc_num'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('acc_num') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('bank') ? ' has-error' : '' }}">
                            <label for="bank" class="col-md-4 control-label">Bank</label>

                            <div class="col-md-6">
                                <input id="bank" type="text" class="form-control" name="bank" value="{{ old('bank') }}" required>

                                @if ($errors->has('bank'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('bank') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('branch') ? ' has-error' : '' }}">
                            <label for="branch" class="col-md-4 control-label">Branch (Only for Account Deposit)</label>

                            <div class="col-md-6">
                                <input id="branch" type="text" class="form-control" name="branch" value="{{ old('branch') }}" required>

                                @if ($errors->has('branch'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('branch') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('relationship') ? ' has-error' : '' }}">
                            <label for="relationship" class="col-md-4 control-label">Relationship</label>

                            <div class="col-md-6">
                                <select class="form-control" id="relationship" name="relationship" required="">
                                    <option disabled="" selected="">Please Select</option>
                                    <option>Parent</option>
                                    <option>Spouse</option>
                                    <option>Friend</option>
                                    <option>Child</option>
                                    <option>Brother</option>
                                    <option>Sister</option>
                                    <option>Grand Parent</option>
                                    <option>Uncle</option>
                                    <option>Aunt</option>
                                    <option>Cousin</option>
                                    <option>Nephew</option>
                                    <option>Niece</option>
                                    <option>In-law</option>
                                    <option>Own</option>
                                </select>

                                @if ($errors->has('relationship'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('relationship') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
