@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form class="form-horizontal" method="post" action="/beneficiaries">
                        {{ csrf_field() }}
                        <div style="color: #333; text-align: center; font-family: 'Yanone Kaffeesatz', sans-serif; width: 100%;font-size: 20px; border-bottom: 1px solid #333;">NEW BENEFICIARY</div><br>

                        <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name without Initials *</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="address" class="col-md-4 control-label">Sri Lankan Address</label>

                            <div class="col-md-6">
                                <input id="address" type="text" class="form-control" name="address" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="contact"  class="col-md-4 control-label">Contact Number</label>

                            <div class="col-md-6">
                                <input id="contact" type="text" onkeypress='return validateQty(event);' class="form-control" name="contact"  required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="accnum" class="col-md-4 control-label">Account Number</label>

                            <div class="col-md-6">
                                <input id="accnum" type="text" onkeypress='return validateQty(event);' class="form-control" name="accnum" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="bank" class="col-md-4 control-label">Bank</label>

                            <div class="col-md-6">
                                <input id="bank" type="text" class="form-control" name="bank" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="branch" class="col-md-4 control-label">Branch (Only for Account Deposit)</label>

                            <div class="col-md-6">
                                <input id="branch" type="text" class="form-control" name="branch" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="relationship" class="col-md-4 control-label">Relationship</label>

                            <div class="col-md-6">
                                <select class="form-control" id="relationship" name="relationship" required="">
                                    <option disabled="" selected="">Please Select</option>
                                    <option>Parent</option>
                                    <option>Spouse</option>
                                    <option>Friend</option>
                                    <option>Child</option>
                                    <option>Brother</option>
                                    <option>Sister</option>
                                    <option>Grand Parent</option>
                                    <option>Uncle</option>
                                    <option>Aunt</option>
                                    <option>Cousin</option>
                                    <option>Nephew</option>
                                    <option>Niece</option>
                                    <option>In-law</option>
                                    <option>Own</option>
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" name="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="panel panel-default">
                <div style="color:rgb(181, 219, 185); text-align: center; font-family: 'Yanone Kaffeesatz', sans-serif; width: 100%;font-size: 18px; ">BENEFICIARIES DETAILS</div><br>
                    <table>
                        <tr style="font-size: 14px; background-color: rgb(239, 239, 239);">
                            <th class="col-md-1"><strong>Relationship</strong></th>
                            <th class="col-md-1"><strong>Name</strong></th>
                            <th class="col-md-1"><strong>Address</strong></th>
                            <th class="col-md-1"><strong>Contact Number</strong></th>
                            <th class="col-md-1"><strong>Account Number</strong></th>
                            <th class="col-md-1"><strong>Bank</strong></th>
                            <th class="col-md-1"><strong>Option</strong></th>
                        </tr>
                        @foreach($beneficiaries as $beneficiary)
                        <tr style="font-size: 12px; border-bottom: 1px solid rgb(202, 228, 226);">
                            <td class="col-md-1">{{ $beneficiary->relationship }}</td>
                            <td class="col-md-1">{{ $beneficiary->name }}</td>
                            <td class="col-md-1">{{ $beneficiary->address }}</td>
                            <td class="col-md-1">{{ $beneficiary->contact }}</td>
                            <td class="col-md-1">{{ $beneficiary->accnum }}</td>
                            <td class="col-md-1">{{ $beneficiary->bank }} - {{ $beneficiary->branch }}</td>
                            <td class="col-md-1">
                                <a href="" class=" btn-danger btn-xs delete_area" >Edit</a> <a href="" class=" btn-danger btn-xs delete_area" >Remove</a>
                            </td>
                        </tr>
                        @endforeach
                    </table><br>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function validateQty(event) {
            var key = window.event ? event.keyCode : event.which;
        if (event.keyCode == 8 || event.keyCode == 46
         || event.keyCode == 37 || event.keyCode == 39) {
            return true;
        }
        else if ( key < 48 || key > 57 ) {
            return false;
        }
        else return true;
        };
    </script>
</div>
@endsection
