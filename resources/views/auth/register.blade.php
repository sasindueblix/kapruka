@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}
                        <div style="color: #333; text-align: center; font-family: 'Yanone Kaffeesatz', sans-serif; width: 100%;font-size: 20px; border-bottom: 1px solid #333;">New Customer Register</div><br>
                        <div class="form-group{{ $errors->has('promotion') ? ' has-error' : '' }}">
                            <label for="promotion" class="col-md-4 control-label">Promotion/Agent Code (if any)</label>

                            <div class="col-md-6">
                                <input id="promotion" type="text" class="form-control" name="promotion" value="{{ old('promotion') }}" autofocus>

                                @if ($errors->has('promotion'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('promotion') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label for="title" class="col-md-4 control-label">Title</label>

                            <div class="col-md-6">
                                <select class="form-control" id="title" name="title">
                                    <option>Mr</option>
                                    <option>Mrs</option>
                                    <option>Miss</option>
                                    <option>Ms</option>
                                    <option>Dr</option>
                                    <option>Other</option>
                                </select>

                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('family_name') ? ' has-error' : '' }}">
                            <label for="family_name" class="col-md-4 control-label">Family Name</label>

                            <div class="col-md-6">
                                <input id="family_name" type="text" class="form-control" name="family_name" value="{{ old('family_name') }}" required autofocus>

                                @if ($errors->has('family_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('family_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Given Name/s</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('citizen') ? ' has-error' : '' }}">
                            <label for="citizen" class="col-md-4 control-label">Citizenship</label>

                            <div class="col-md-6">
                                <select class="form-control" id="citizen" name="citizen" required="">
                                    <option disabled="">Please Select</option>
                                    <option>Australian</option>
                                    <option>Sri Lankan</option>
                                    <option>New Zealand</option>
                                    <option>Dual Citizenship</option>
                                    <option>Other</option>
                                </select>

                                @if ($errors->has('citizen'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('citizen') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('birthday') ? ' has-error' : '' }}">
                            <label for="birthday" class="col-md-4 control-label">Date of Birth</label>

                            <div class="col-md-6">
                                <input id="birthday" type="date" class="form-control" name="birthday" value="{{ old('birthday') }}" required autofocus>

                                @if ($errors->has('birthday'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('birthday') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('birth_place') ? ' has-error' : '' }}">
                            <label for="birth_place" class="col-md-4 control-label">Place of Birth</label>

                            <div class="col-md-6">
                                <input id="birth_place" type="text" class="form-control" name="birth_place" value="{{ old('birth_place') }}" required autofocus>

                                @if ($errors->has('birth_place'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('birth_place') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('occupation') ? ' has-error' : '' }}">
                            <label for="occupation" class="col-md-4 control-label">Occupation</label>

                            <div class="col-md-6">
                                <input id="occupation" type="text" class="form-control" name="occupation" value="{{ old('occupation') }}" required autofocus>

                                @if ($errors->has('occupation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('occupation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('telephone') ? ' has-error' : '' }}">
                            <label for="telephone" class="col-md-4 control-label">Telephone</label>

                            <div class="col-md-6">
                                <input id="telephone" type="text" onkeypress='return validateQty(event);' class="form-control" name="telephone" value="{{ old('telephone') }}" required autofocus>

                                @if ($errors->has('telephone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('telephone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
                            <label for="mobile" class="col-md-4 control-label">Mobile</label>

                            <div class="col-md-6">
                                <input id="mobile" type="text" onkeypress='return validateQty(event);' class="form-control" name="mobile" value="{{ old('mobile') }}" required>

                                @if ($errors->has('mobile'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('street_num') ? ' has-error' : '' }}">
                            <label for="street_num" class="col-md-4 control-label">Street Number</label>

                            <div class="col-md-6">
                                <input id="street_num" type="text" class="form-control" name="street_num" value="{{ old('street_num') }}" required>

                                @if ($errors->has('street_num'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('street_num') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('street_name') ? ' has-error' : '' }}">
                            <label for="street_name" class="col-md-4 control-label">Street Name</label>

                            <div class="col-md-6">
                                <input id="street_name" type="text" class="form-control" name="street_name" value="{{ old('street_name') }}" required>

                                @if ($errors->has('street_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('street_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('street_type') ? ' has-error' : '' }}">
                            <label for="street_type" class="col-md-4 control-label">Street Type</label>

                            <div class="col-md-6">
                                <select class="form-control" id="street_type" name="street_type" required="">
                                    <option disabled="" selected="">Please Select</option>
                                    <option value="Esplanade">Esplanade</option>
                                    <option value="Access">Access</option>
                                    <option value="Avenue">Avenue</option>
                                    <option value="Boulevard">Boulevard</option>
                                    <option value="Circle">Circle</option>
                                    <option value="CL">Close</option>
                                    <option value="Crescent">Crescent</option>
                                    <option value="Court">Court</option>
                                    <option value="Drive">Drive</option>
                                    <option value="Garden">Garden</option>
                                    <option value="Grove">Grove</option>
                                    <option value="Highway">Highway</option>
                                    <option value="Lane">Lane</option>
                                    <option value="Parade">Parade</option>
                                    <option value="Place">Place</option>
                                    <option value="Road">Road</option>
                                    <option value="Street">Street</option>
                                    <option value="Strip">Strip</option>
                                    <option value="Square">Square</option>
                                    <option value="Terrace">Terrace</option>
                                    <option value="Way">Way</option>
                                    <option value="Circuit">Circuit</option>
                                    <option value="Approach">Approach</option>
                                    <option value="Loop">Loop</option>
                                    <option value="Trail">Trail</option>
                                    <option value="Chase">Chase</option>
                                </select>

                                @if ($errors->has('street_type'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('street_type') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('post_code') ? ' has-error' : '' }}">
                            <label for="post_code" class="col-md-4 control-label">Post Code</label>

                            <div class="col-md-6">
                                <input id="post_code" type="text" class="form-control" name="post_code" value="{{ old('post_code') }}" required>

                                @if ($errors->has('post_code'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('post_code') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('suburb') ? ' has-error' : '' }}">
                            <label for="suburb" class="col-md-4 control-label">Suburb</label>

                            <div class="col-md-6">
                                <input id="suburb" type="text" class="form-control" name="suburb" value="{{ old('suburb') }}" required>

                                @if ($errors->has('suburb'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('suburb') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('satate') ? ' has-error' : '' }}">
                            <label for="satate" class="col-md-4 control-label">State</label>

                            <div class="col-md-6">
                                <select class="form-control" id="state" name="state" required="">
                                    <option disabled="" selected="">Please Select</option>
                                    <option name="NSW">NSW - New South Wales</option>
                                    <option name="VIC">VIC - Victoria</option>
                                    <option name="QLD">QLD - Queensland</option>
                                    <option name="WA">WA - Western Australia</option>
                                    <option name="SA">SA - South Australia</option>
                                    <option name="TAS">TAS - Tasmania</option>
                                    <option name="ACT">ACT - Australian Capital Territory</option>
                                    <option name="NT">NT - Northern Territory</option>
                                </select>

                                @if ($errors->has('satate'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('satate') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('id_type') ? ' has-error' : '' }}">
                            <label for="id_type" class="col-md-4 control-label">ID Type</label>

                            <div class="col-md-6">
                                <select class="form-control" id="id_type" name="id_type" required="">
                                    <option disabled="" selected="">Please Select</option>
                                    <option>Passport</option>
                                    <option>Driver’s License</option>
                                </select>

                                @if ($errors->has('id_type'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('id_type') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('id_country') ? ' has-error' : '' }}">
                            <label for="id_country" class="col-md-4 control-label">Issued Country</label>

                            <div class="col-md-6">
                                <select class="form-control" id="id_country" name="id_country" required="">
                                    <option disabled="" selected="">Please Select</option>
                                    <option>Sri Lanka</option>
                                    <option>Australia</option>
                                    <option>New Zealand</option>
                                    <option>Other</option>
                                </select>

                                @if ($errors->has('id_country'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('id_num') ? ' has-error' : '' }}">
                            <label for="id_num" class="col-md-4 control-label">ID Number</label>

                            <div class="col-md-6">
                                <input id="id_num" type="text" class="form-control" name="id_num" value="{{ old('id_num') }}" required>

                                @if ($errors->has('id_num'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('id_num') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('id_exp') ? ' has-error' : '' }}">
                            <label for="id_exp" class="col-md-4 control-label">Expire Date</label>

                            <div class="col-md-6">
                                <input id="id_exp" type="date" class="form-control" name="id_exp" value="{{ old('id_exp') }}" required>

                                @if ($errors->has('id_exp'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('id_exp') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Email Address</label>

                            <div class="col-md-6">
                                <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function validateQty(event) {
    var key = window.event ? event.keyCode : event.which;
if (event.keyCode == 8 || event.keyCode == 46
 || event.keyCode == 37 || event.keyCode == 39) {
    return true;
}
else if ( key < 48 || key > 57 ) {
    return false;
}
else return true;
};
    </script>
</div>
@endsection
