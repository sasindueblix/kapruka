@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="/deposit">
                        {{ csrf_field() }}
                        <div style="color: #333; text-align: center; font-family: 'Yanone Kaffeesatz', sans-serif; width: 100%;font-size: 20px; border-bottom: 1px solid #333;">Deposit</div><br>
                        
                        <div class="form-group{{ $errors->has('beneficiary_id') ? ' has-error' : '' }}">
                            <label for="beneficiary_id" class="col-md-4 control-label">Beneficiary's Name</label>

                            <div class="col-md-6">
                                <select class="form-control" id="beneficiary_id" name="beneficiary_id" required="">
                                    <option disabled="" selected="">Please Select</option>
                                    @foreach($beneficiaries as $beneficiary)
                                        <option value="{{ $beneficiary->id }}">{{ $beneficiary->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
                            <label for="amount" class="col-md-4 control-label">Amount Deposited</label>

                            <div class="col-md-6">
                                <input id="amount" type="text" class="form-control" name="amount" autofocus>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('source') ? ' has-error' : '' }}">
                            <label for="source" class="col-md-4 control-label">Source of Funds</label>

                            <div class="col-md-6">
                                <select class="form-control" id="source" name="source" required="">
                                    <option disabled="" selected="">Please Select</option>
                                    <option>Personal Savings</option>
                                    <option>Business Activities</option>
                                    <option>Loan</option>
                                    <option>Family Money</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('purpose') ? ' has-error' : '' }}">
                            <label for="purpose" class="col-md-4 control-label">Purpose</label>

                            <div class="col-md-6">
                                <select class="form-control" id="purpose" name="purpose" required="">
                                    <option disabled="" selected="">Please Select</option>
                                    <option>Family Support</option>
                                    <option>Medical Expenses</option>
                                    <option>Personal Travels and Tours</option>
                                    <option>Repayments of Loans</option>
                                    <option>Gift</option>
                                    <option>Special Occasion</option>
                                    <option>Investment</option>
                                    <option>Goods Purchased</option>
                                    <option>Other</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('method') ? ' has-error' : '' }}">
                            <label for="method" class="col-md-4 control-label">Payment Method</label>

                            <div class="col-md-6">
                                <select class="form-control" id="method" name="method" required="">
                                    <option disabled="" selected="">Please Select</option>
                                    <option>Account Deposit</option>
                                    <option>Over The Counter</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="panel panel-default">
                <div style="color:rgb(181, 219, 185); text-align: center; font-family: 'Yanone Kaffeesatz', sans-serif; width: 100%;font-size: 18px; ">Deposits History</div><br>
                    <table>
                        <tr style="font-size: 14px; background-color: rgb(239, 239, 239);">
                            <th class="col-md-2"><strong>Date</strong></th>
                            <th class="col-md-2"><strong>Beneficiary</strong></th>
                            <th class="col-md-2"><strong>Amount</strong></th>
                            <th class="col-md-2"><strong>Purpose</strong></th>
                            <th class="col-md-2"><strong>Payment Method</strong></th>
                        </tr>
                        @foreach($deposits as $deposit)
                            <tr style="font-size: 12px; border-bottom: 1px solid rgb(202, 228, 226);">
                                <td class="col-md-2">{{ $deposit->created_at->format('m-d-Y')  }}</td>
                                <td class="col-md-2">{{ $deposit->beneficiary->name }}</td>
                                <td class="col-md-2">{{  $deposit->amount }}</td>
                                <td class="col-md-2">{{  $deposit->purpose }}</td>
                                <td class="col-md-2">{{  $deposit->method }}</td>
                            </tr>
                        @endforeach
                    </table><br>
            </div>

        </div>
    </div>
</div>
@endsection
