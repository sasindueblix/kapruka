<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/profile', 'ProfileController@index')->name('profile');
Route::get('/account', 'AccountController@index')->name('account');
Route::post('/deposit', 'DepositController@insert');
Route::get('/deposit', 'DepositController@index');
Route::post('/beneficiaries', 'BeneficiaryController@insert');
Route::get('/beneficiaries', 'BeneficiaryController@getData');
Route::get('/morebeneficiaries', 'MoreBeneficiaryController@index')->name('morebeneficiaries');

//Admin
Route::get('protected', ['middleware' => ['auth', 'admin'], function() {
    return "this page requires that you be logged in and an Admin";
}]);

Route::prefix('admin')->group(function(){
	Route::get('/', 'AdminController@index')->name('admin.dashboard');
	Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
	Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
	Route::get('/users', 'UserController@index')->name('admin.users');
});
