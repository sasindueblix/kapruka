<?php

namespace App;

use App\Http\Controllers\AdminDashboardController;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    const ADMIN = 'admin';
    const USER = 'user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'promotion', 'title', 'family_name', 'citizen', 'birthday', 'birth_place', 'occupation', 'telephone', 'mobile', 'street_num', 'street_name', 'street_type', 'post_code', 'suburb', 'state', 'id_type', 'id_country', 'id_num', 'id_exp'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @return bool
     */
    public function isAdmin()
    {
        return $this->role === self::ADMIN;
    }
}
