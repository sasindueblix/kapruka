<?php

namespace App\Http\Controllers;

use App\Beneficiary;
use App\Deposit;
use Illuminate\Http\Request;
use DB;

class DepositController extends Controller
{

    public function index()
    {
        $userId = \Auth::user()->id;

        $deposits = Deposit::with('beneficiary')->where('user_id', $userId)->get();
        $beneficiaries = Beneficiary::where('user_id', $userId)->get();

        return view('deposit', ['beneficiaries' => $beneficiaries, 'deposits' => $deposits]);
    }

    public function insert(Request $request)
    {
        $amount = $request->input('amount');
        $source = $request->input('source');
        $purpose = $request->input('purpose');
        $method = $request->input('method');
        $beneficiary_id = $request->input('beneficiary_id');
        $user_id = auth()->user()->getAuthIdentifier();

        $data = array('amount'=>$amount, 'source'=>$source, 'purpose'=>$purpose, 'method'=>$method, 'beneficiary_id'=>$beneficiary_id, 'user_id'=>$user_id );

        DB::table('deposits')->insert($data);

        return redirect()->back();
    }
}
