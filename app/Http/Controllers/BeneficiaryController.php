<?php

namespace App\Http\Controllers;

use App\Beneficiary;
use Illuminate\Http\Request;
use DB;

class BeneficiaryController extends Controller
{

    function insert(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $data = $request->all();
        $data['user_id'] = auth()->user()->getAuthIdentifier();

        Beneficiary::create($data);

    	$this->getData();
    	return redirect()->back();
    }

    function getData()
    {
        $beneficiaries = Beneficiary::where('user_id', \Auth::user()->id)->get();

        return view('beneficiaries', ['beneficiaries' => $beneficiaries]);

    }

}
