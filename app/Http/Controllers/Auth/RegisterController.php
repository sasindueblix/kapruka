<?php
namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'promotion' => $data['promotion'],
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'title' => $data['title'],
            'family_name' => $data['family_name'],
            'citizen' => $data['citizen'],
            'birthday' => $data['birthday'],
            'birth_place' => $data['birth_place'],
            'occupation' => $data['occupation'],
            'telephone' => $data['telephone'],
            'mobile' => $data['mobile'],
            'street_num' => $data['street_num'],
            'street_name' => $data['street_name'],
            'street_type' => $data['street_type'],
            'post_code' => $data['post_code'],
            'suburb' => $data['suburb'],
            'state' => $data['state'],
            'id_type' => $data['id_type'],
            'id_country' => $data['id_country'],
            'id_num' => $data['id_num'],
            'id_exp' => $data['id_exp'], 
        ]);
    }
}
