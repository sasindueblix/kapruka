<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Beneficiary extends Model
{
    protected $fillable = ['name', 'address', 'contact', 'accnum', 'bank', 'branch', 'relationship', 'user_id' ];
    public $timestamps = false;
}
