<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deposit extends Model
{

    public function beneficiary()
    {
        return $this->belongsTo(Beneficiary::class);
    }
}
